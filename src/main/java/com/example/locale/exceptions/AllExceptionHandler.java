package com.example.locale.exceptions;


import com.example.locale.dto.BaseErrorDto;
import com.example.locale.dto.BaseResDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AllExceptionHandler {
    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<?> handleDataNotFoundException(DataNotFoundException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new BaseResDto<>(BaseErrorDto.builder()
                .code(-1)
                .msg(e.getLocalizedMessage())
                .build()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataParseException.class)
    public ResponseEntity<?> handleDataParseException(DataParseException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new BaseResDto<>(BaseErrorDto.builder()
                .code(-1)
                .msg(e.getLocalizedMessage())
                .build()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        e.printStackTrace();
        BindingResult bindingResult = e.getBindingResult();
        String msg = "";
        for (ObjectError error : bindingResult.getAllErrors()) {
            //    sb.append('[').append(error).append("] ");
            msg = error.toString();
        }
        int startIndex = msg.indexOf("default message");
        msg = msg.substring(startIndex);
        return new ResponseEntity<>(new BaseResDto<>(BaseErrorDto.builder()
                .code(-1)
                .msg(msg)
                .build()),
                HttpStatus.BAD_REQUEST);
    }
}
