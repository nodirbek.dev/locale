package com.example.locale.exceptions;

public class DataParseException extends RuntimeException{
    public DataParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
