package com.example.locale.exceptions;

import com.example.locale.dto.BaseErrorDto;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message) {
        super(message);
    }

}
