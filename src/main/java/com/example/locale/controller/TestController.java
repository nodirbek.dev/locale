package com.example.locale.controller;

import com.example.locale.dto.BaseResDto;
import com.example.locale.dto.CardReqDto;
import com.example.locale.dto.Family;
import com.example.locale.dto.TestDateDto;
import com.example.locale.service.ReadJsonDBService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TestController {
    private final MessageSource messageSource;
    private final ReadJsonDBService readJsonDBService;

    @PostMapping("/card")
    public ResponseEntity<BaseResDto<?>> postCard(@RequestBody @Valid CardReqDto cardReqDto) {
        return ResponseEntity.ok(BaseResDto.<CardReqDto>builder()
                .success(true)
                .data(cardReqDto)
                .build());
    }

    @GetMapping("hi")
    public String sayHello() {
        return messageSource.getMessage("greeting", null, LocaleContextHolder.getLocale());
    }

    @GetMapping("/family")
    public ResponseEntity<BaseResDto<?>> getFamily() {
        String parents = messageSource.getMessage("parents", null, LocaleContextHolder.getLocale());
        String sister = messageSource.getMessage("sister", null, LocaleContextHolder.getLocale());
        String grandparents = messageSource.getMessage("grandparents", null, LocaleContextHolder.getLocale());
        String brother = messageSource.getMessage("brother", null, LocaleContextHolder.getLocale());
        Family familyDto = new Family();
        familyDto.setA(parents);
        familyDto.setB(sister);
        familyDto.setC(grandparents);
        familyDto.setD(brother);

        return ResponseEntity.ok(BaseResDto.<Family>builder()
                .success(true)
                .data(familyDto)
                .build());
    }

    @PostMapping("/date")
    public String postDate(@RequestBody TestDateDto testDateDto) {
        LocalDate from = testDateDto.getFrom();
        LocalDate to = testDateDto.getTo();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTimeFrom = LocalDateTime.parse(from.toString() + " 00:00:01", formatter);
        LocalDateTime localDateTimeTo = LocalDateTime.parse(to.toString() + " 00:00:01", formatter);
        System.out.println(localDateTimeFrom);
        System.out.println(localDateTimeTo);
        if (from.isAfter(to))
            return "Not Valid";
        else
            return "Valid";
        //return from + " " + to;
    }

    //@ApiResponses(value = {@ApiResponse(responseCode = "200", useReturnTypeSchema = true)})
    @GetMapping("/jsonb")
    public ResponseEntity<BaseResDto<?>> readJsonb(@RequestParam Integer id) {
        return readJsonDBService.getJsonObjectFromDb(id);
    }
}
