package com.example.locale.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestTableRep extends JpaRepository<TestTableOne, Integer> {
}
