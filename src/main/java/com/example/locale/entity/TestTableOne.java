package com.example.locale.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "test_table1")
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class,
        defaultForType = JsonNode.class)
public class TestTableOne {
    @Id
    private Integer id;
    @Type(type = "jsonb")
    @Column(name = "data", columnDefinition = "jsonb")
    private JsonNode jsonData;
    @Column(name = "is_array")
    private Boolean isArray;

    public Boolean getArray() {
        return Objects.requireNonNullElse(isArray, false);
    }
}
