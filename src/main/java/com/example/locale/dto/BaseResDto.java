package com.example.locale.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResDto<T> {
    private Boolean success;
    private String msg;
    private T data;
    private BaseErrorDto error;

    public BaseResDto(BaseErrorDto error) {
        this.success=false;
        this.error = error;
    }
}
