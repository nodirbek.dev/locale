package com.example.locale.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitEposDto {
    private Boolean wage;
    @JsonProperty("limit_min")
    private Long limitMin;
    @JsonProperty("limit_max")
    private Long limitMax;
    private Double commission;
    private String port;
    @JsonProperty("merchant_id")
    private String merchantId;
    @JsonProperty("terminal_id")
    private String terminalId;
}
