package com.example.locale.dto;

import com.example.locale.constraints.ValidCard;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CardReqDto {
    @ValidCard
    private String card;
}
