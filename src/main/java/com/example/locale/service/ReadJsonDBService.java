package com.example.locale.service;

import com.example.locale.dto.BaseErrorDto;
import com.example.locale.dto.BaseResDto;
import com.example.locale.dto.DebitEposDto;
import com.example.locale.entity.TestTableOne;
import com.example.locale.entity.TestTableRep;
import com.example.locale.exceptions.DataNotFoundException;
import com.example.locale.exceptions.DataParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReadJsonDBService {
    private final TestTableRep testTableRep;

    public ResponseEntity<BaseResDto<?>> getJsonObjectFromDb(Integer id) {
        Optional<TestTableOne> testTableOneOptional = testTableRep.findById(id);
        if (testTableOneOptional.isEmpty()) {
            throw new DataNotFoundException("Data not found");
        }

        TestTableOne testTableOne = testTableOneOptional.get();
        Boolean isArray = testTableOne.getArray();
        JsonNode jsonData = testTableOne.getJsonData();
        ObjectMapper mapper = new ObjectMapper();
        if (isArray) {
            ObjectReader reader = mapper.readerFor(new TypeReference<List<DebitEposDto>>() {
            });
            List<DebitEposDto> debitEposDtoList;
            try {
                debitEposDtoList = reader.readValue(jsonData);
                for (DebitEposDto debitEposDto : debitEposDtoList) {
                    if(debitEposDto.getWage())
                        System.out.println(debitEposDto);
                }
                return ResponseEntity
                        .ok(BaseResDto.builder()
                                .success(true)
                                .msg("list")
                                .data(debitEposDtoList)
                                .error(BaseErrorDto.builder().build())
                                .build()
                        );

            } catch (IOException e) {
                throw new DataParseException("Parse: error list", e);
            }
        } else {
            try {
                DebitEposDto debitEposDto=mapper.treeToValue(jsonData, DebitEposDto.class);
                return ResponseEntity
                        .ok(BaseResDto.builder()
                                .success(true)
                                .msg("object")
                                .data(debitEposDto)
                                .error(BaseErrorDto.builder().build())
                                .build()
                        );
            } catch (JsonProcessingException e) {
                throw new DataParseException("Parse: error object", e);
            }
        }
    }
}
